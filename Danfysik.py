#	"$Name:  $";
#	"$Header:  $";
#=============================================================================
#
# file :        Danfysik.py
#
# description : Python source for the Danfysik and its commands.
#                The class is derived from Device. It represents the
#                CORBA servant object which will be accessed from the
#                network. All commands which can be executed on the
#                Danfysik are implemented in this file.
#
# project :     TANGO Device Server
#
# $Author:      pskorek@cells.es
#
# $Revision:  $
#
# $Log:  $
#
# copyleft :    European Synchrotron Radiation Facility
#               BP 220, Grenoble 38043
#               FRANCE
#
#=============================================================================
#   This file is generated by POGO
#   (Program Obviously used to Generate tango Object)
#
#         (c) - Software Engineering Group - ESRF
#=============================================================================

import PyTango, sys, traceback, time
import threading
import state

VOLTAGE_READBACK_REGISTER_AD = 2
VOLTAGE_READBACK_FACTOR = 10

CURRENT_READBACK_REGISTER_AD = 8
CURRENT_READBACK_FACTOR = 100

CURRENT_SETPOINT_REGISTER_AD = 19
CURRENT_SETPOINT_FACTOR = 1000

AMBIENT_TEMPERATURE_REGISTER_AD = 1
AMBIENT_TEMPERATURE_FACTOR = 1

#==================================================================
#   Danfysik Class Description:
#
#   The tailor is rich and my mother is in the kitchen.
#
#==================================================================
class Danfysik(PyTango.Device_3Impl):

#--------- Add you global variables here --------------------------

#------------------------------------------------------------------
#   Device constructor
#------------------------------------------------------------------
    def __init__(self,cl, name):
        PyTango.Device_3Impl.__init__(self,cl,name)
        self._important_logs = []
        self.dp = None
        self.statusDict = state.DanfysikStatusDict
        self._errors = []
        self._remoteMode = None
        self._haveSerialLineConn = False
        self._serialLineTrace = []
        Danfysik.init_device(self)

#------------------------------------------------------------------
#   Device destructor
#------------------------------------------------------------------
    def delete_device(self):
        self.info_stream("[Device delete_device method] for device %s"%(self.get_name()))

#------------------------------------------------------------------
#   Device initialization
#------------------------------------------------------------------
    def init_device(self):
        self.debug_stream("In %s::init_device()"%(self.get_name()))
        self.changeState(PyTango.DevState.INIT)
        self.get_device_properties(self.get_device_class())
        
        self.info_stream("Serial lines is %s (%s)"%(self.SerialLine,type(self.SerialLine)))
        self._linkSerial()

        self.set_change_event('State', True, False)
        self.set_change_event('Status', True, False)
#        self.set_change_event('Current', True, False)
#        self.set_change_event('Voltage', True, False)
        self.set_change_event('RemoteMode', True, False)
        self.set_change_event('Errors', True, False)
        self.set_change_event('SerialLineTrace', True, False)

    def _linkSerial(self):
        try:
            if self.dp == None:
                self.dp = PyTango.DeviceProxy(self.SerialLine)
            self.dp.DevSerWriteString('S1\r')
            self.waiting=threading.Event()
            self.waiting.wait(0.1)
            if not self.dp == None:
                self.statusString = self.dp.DevSerReadRaw().strip('\n\r')
                if len(self.statusString) > 0:
                    if self.statusString[0] == '':
                        self.changeState(PyTango.DevState.FAULT)
                        self.cleanAllImportantLogs()
                        self.addStatusMsg("Serial line is not responding",False)
                        if self._haveSerialLineConn:
                            self.append2SerialLineTrace("Serial line is not responding")
                            self._haveSerialLineConn = False
                    elif self.statusString[0] == '!':
                        self.changeState(PyTango.DevState.OFF)
                        if not self._haveSerialLineConn:
                            self.append2SerialLineTrace("Serial line recovered")
                            self._haveSerialLineConn = True
                    else:
                        self.changeState(PyTango.DevState.ON)
                        if not self._haveSerialLineConn:
                            self.append2SerialLineTrace("Serial line recovered")
                            self._haveSerialLineConn = True
                        self.dp.DevSerWriteString('ERRT\r')
                        self.dp.DevSerFlush(2)
                else:
                    self.changeState(PyTango.DevState.FAULT)
                    self.cleanAllImportantLogs()
                    msg = "Cannot link with serial line"
                    self.addStatusMsg(msg,True)
        except Exception,e:
            self.dp = None
            self.changeState(PyTango.DevState.FAULT)
            self.cleanAllImportantLogs()
            msg = "Exception when try to link with serial line"
            self.addStatusMsg(msg,True)
            self.debug_stream(msg+": %s"%(e))
            traceback.format_exc()

#------------------------------------------------------------------
#   Always excuted hook method
#------------------------------------------------------------------
    def always_executed_hook(self):
        self.debug_stream("In %s::always_excuted_hook()"%(self.get_name()))

#==================================================================
#
#   Danfysik read/write attribute methods
#
#==================================================================
#------------------------------------------------------------------    
#   Read Attribute Hardware
#------------------------------------------------------------------
    def read_attr_hardware(self,data):
        self.debug_stream("In %s::read_attr_hardware()"%(self.get_name()))

#------------------------------------------------------------------
#   Read Current attribute
#------------------------------------------------------------------
    def read_Current(self, attr):
        self.debug_stream("In %s::read_Current()"%(self.get_name()))
        #   Add your own code here
        self.dp.DevSerWriteString('AD %d\r'%CURRENT_READBACK_REGISTER_AD)
        self.waiting=threading.Event()
        self.waiting.wait(0.1) 
        res = self.dp.DevSerReadRaw().strip('\n\r')
        if not self._IsAnswerError(res):
            attr_Current_read = float(res)/CURRENT_READBACK_FACTOR
            attr.set_value(attr_Current_read)
        else:
            attr.set_quality(PyTango.AttrQuality.ATTR_INVALID)

    def is_Current_allowed(self, req_type):
        if self.get_state() in [PyTango.DevState.INIT] or\
           self.dp == None:
            #    End of Generated Code
            #    Re-Start of Generated Code
            return False
        return True

#------------------------------------------------------------------
#   Read StateCode64 attribute
#------------------------------------------------------------------
    def read_StateCode64(self, attr=None):
        self.debug_stream("In %s::read_StateCode64()"%(self.get_name()))
        # Add your own code here

        if self.get_state() == PyTango.DevState.FAULT and self.dp == None:
            #TODO: distinguish between communication fault and PS fault
            if attr != None:
                attr.set_quality(PyTango.AttrQuality.ATTR_INVALID)
            return

        self.dp.DevSerWriteString('S1\r')
        self.waiting=threading.Event()
        self.waiting.wait(0.1) 
        res = self.dp.DevSerReadRaw().strip('\n\r')
        if not self._IsAnswerError(res):
            temp=[]
            for i in range(len(res)):
                if res[i]=='!': temp.append('1')
                elif res[i]=='.': temp.append('0')
                else:
                    self.error_stream("Serial line status answer has an "\
                                      "invalid character '%s' in position %d"
                                      %(res[i],i))
            stateCode64_bitstr = ''.join(temp)
            try:
                attr_StateCode64_read = int('0b'+stateCode64_bitstr,2)
            except:
                attr_StateCode64_read = 0
            self.debug_stream("StateCode64 %s = 0b%s = %d"%(res,stateCode64_bitstr,
                                                            attr_StateCode64_read))
            
            if attr==None:
                return attr_StateCode64_read
            else:
                attr.set_value(attr_StateCode64_read)
        elif attr==None:
            return None
        else:
            attr.set_quality(PyTango.AttrQuality.ATTR_INVALID)

#------------------------------------------------------------------
#   Read CurrentSetpoint attribute
#------------------------------------------------------------------
    def read_CurrentSetpoint(self, attr):
        self.debug_stream("In %s::read_CurrentSetpoint()"%(self.get_name()))
        # Add your own code here

        #self.dp.DevSerWriteString('RA\r')
        self.dp.DevSerWriteString('AD %d\r'%CURRENT_SETPOINT_REGISTER_AD)
        self.waiting=threading.Event()
        self.waiting.wait(0.1) 
        res = self.dp.DevSerReadRaw().strip('\n\r')
        if not self._IsAnswerError(res):
            attr_CurrentSetpoint_read = int(res)
            attr_CurrentSetpoint_read = float(attr_CurrentSetpoint_read)/CURRENT_SETPOINT_FACTOR
            attr.set_value(attr_CurrentSetpoint_read)
        else:
            attr.set_quality(PyTango.AttrQuality.ATTR_INVALID)

#------------------------------------------------------------------
#   Write CurrentSetpoint attribute
#------------------------------------------------------------------
    def write_CurrentSetpoint(self, attr):
        self.debug_stream("In %s::write_CurrentSetpoint()"%(self.get_name()))
        #   Add your own code here

        self.CurrentSetpoint = attr.get_write_value()*CURRENT_SETPOINT_FACTOR
        self.dp.DevSerWriteString('DA 0 '+str(self.CurrentSetpoint)+'\r')
        self.waiting=threading.Event()
        self.waiting.wait(0.1) 
        res = self.dp.DevSerReadRaw().strip('\n\r')
        if not self._IsAnswerError(res):
            self.push_change_event("CurrentSetpoint", self.CurrentSetpoint)

    def is_CurrentSetpoint_allowed(self, req_type):
        if self.get_state() in [PyTango.DevState.INIT] or\
           self.dp == None:
            #    End of Generated Code
            #    Re-Start of Generated Code
            return False
        return True

#------------------------------------------------------------------
#   Read Voltage attribute
#------------------------------------------------------------------
    def read_Voltage(self, attr):
        self.debug_stream("In %s::read_Voltage()"%(self.get_name()))
        #   Add your own code here

        self.dp.DevSerWriteString('AD %d\r'%VOLTAGE_READBACK_REGISTER_AD)
        self.waiting=threading.Event()
        self.waiting.wait(0.1) 
        res = self.dp.DevSerReadRaw().strip('\n\r')
        if not self._IsAnswerError(res):
            attr_Voltage_read = float(res)/VOLTAGE_READBACK_FACTOR
            attr.set_value(attr_Voltage_read)
        else:
            attr.set_quality(PyTango.AttrQuality.ATTR_INVALID)

    def is_Voltage_allowed(self, req_type):
        if self.get_state() in [PyTango.DevState.INIT] or\
           self.dp == None:
            #    End of Generated Code
            #    Re-Start of Generated Code
            return False
        return True

#------------------------------------------------------------------
#   Read AmbientTemperature attribute
#------------------------------------------------------------------
    def read_AmbientTemperature(self, attr):
        self.debug_stream("In %s::read_AmbientTemperature()"%(self.get_name()))
        #   Add your own code here

        self.dp.DevSerWriteString('AD %d\r'%AMBIENT_TEMPERATURE_REGISTER_AD)
        self.waiting=threading.Event()
        self.waiting.wait(0.1) 
        res = self.dp.DevSerReadRaw().strip('\n\r')
        if not self._IsAnswerError(res):
            attr_Voltage_read = float(res)/AMBIENT_TEMPERATURE_FACTOR
            attr.set_value(attr_Voltage_read)
        else:
            attr.set_quality(PyTango.AttrQuality.ATTR_INVALID)

    def is_AmbientTemperature_allowed(self, req_type):
        if self.get_state() in [PyTango.DevState.INIT] or\
           self.dp == None:
            #    End of Generated Code
            #    Re-Start of Generated Code
            return False
        return True

#------------------------------------------------------------------
#   Read CurrentSlewrate attribute
#------------------------------------------------------------------
    def read_CurrentSlewrate(self, attr):
        self.debug_stream("In %s::read_CurrentSlewrate()"%(self.get_name()))
        #   Add your own code here

        self.dp.DevSerWriteString('DA 1\r')
        self.waiting=threading.Event()
        self.waiting.wait(0.1) 
        res = self.dp.DevSerReadRaw().strip('\n\r')
        if not self._IsAnswerError(res):
            self.v = int(res.split(' ')[1])
            attr_CurrentSlewrate_read = float(self.v)/1000
            attr.set_value(attr_CurrentSlewrate_read)
        else:
            attr.set_quality(PyTango.AttrQuality.ATTR_INVALID)

#------------------------------------------------------------------
#   Write CurrentSlewrate attribute
#------------------------------------------------------------------
    def write_CurrentSlewrate(self, attr):
        self.debug_stream("In %s::write_CurrentSlewrate()"%(self.get_name()))
        #   Add your own code here

        self.CurrentSlewrate = attr.get_write_value()*1000
        self.dp.DevSerWriteString('DA 1 '+str(self.CurrentSlewrate)+'\r')
        self.waiting=threading.Event()
        self.waiting.wait(0.1) 
        res = self.dp.DevSerReadRaw().strip('\n\r')
        if not self._IsAnswerError(res):
            self.push_change_event("CurrentSlewrate", self.CurrentSlewrate)

    def is_CurrentSlewrate_allowed(self, req_type):
        if self.get_state() in [PyTango.DevState.INIT] or\
           self.dp == None:
            #    End of Generated Code
            #    Re-Start of Generated Code
            return False
        return True

#------------------------------------------------------------------
#   Read RemoteMode attribute
#------------------------------------------------------------------
    def read_RemoteMode(self, attr=None):
        self.debug_stream("In %s::read_RemoteMode()"%(self.get_name()))
        #   Add your own code here

        if self._remoteMode == None:
            attr.set_quality(PyTango.AttrQuality.ATTR_INVALID)
        else:
            attr.set_value(self._remoteMode)
            
#        self.dp.DevSerWriteString('CMD\r')
#        self.waiting=threading.Event()
#        self.waiting.wait(0.1) 
#        res = self.dp.DevSerReadRaw().strip('\n\r')
#        if not self._IsAnswerError(res):
#            if res=='REM ': attr_RemoteMode_read=True
#            else: attr_RemoteMode_read=False
#            if attr==None:
#                return attr_RemoteMode_read
#            else:
#                attr.set_value(attr_RemoteMode_read)
#        else:
#            if attr==None:
#                return False
#            else:
#                attr.set_quality(PyTango.AttrQuality.ATTR_INVALID)

#------------------------------------------------------------------
#   Write RemoteMode attribute
#------------------------------------------------------------------
    def write_RemoteMode(self, attr):
        self.debug_stream("In %s::write_RemoteMode()"%(self.get_name()))
        #   Add your own code here

        self.value = attr.get_write_value()
        if self.value==True: self.Remote='REM '
        else: self.Remote='LOC '
        self.dp.DevSerWriteString(str(self.Remote)+'\r')
        self.waiting=threading.Event()
        self.waiting.wait(0.1) 
        res = self.dp.DevSerReadRaw().strip('\n\r')
        if not self._IsAnswerError(res):
            self.push_change_event("RemoteMode", self.value)

    def is_RemoteMode_allowed(self, req_type):
        if self.get_state() in [PyTango.DevState.INIT] or\
           self.dp == None:
            #    End of Generated Code
            #    Re-Start of Generated Code
            return False
        return True

#------------------------------------------------------------------
#   Read Errors attribute
#------------------------------------------------------------------
    def read_Errors(self, attr):
        self.debug_stream("In %s::read_Errors()"%(self.get_name()))
        #   Add your own code here

        attr.set_value(self._errors,len(self._errors))

#------------------------------------------------------------------
#   Read SerialLineTrace attribute
#------------------------------------------------------------------
    def read_SerialLineTrace(self, attr):
        self.debug_stream("In %s::read_SerialLineTrace()"%(self.get_name()))
        #   Add your own code here

        attr.set_value(self._serialLineTrace,len(self._serialLineTrace))

#==================================================================
#
#   Danfysik command methods
#
#==================================================================

#------------------------------------------------------------------
#   Off command:
#
#   Description:
#------------------------------------------------------------------
    def Off(self):
        self.debug_stream("In %s::Off()"%(self.get_name()))
        #   Add your own code here
        self.dp.DevSerWriteString('F\r')
 
#------------------------------------------------------------------
#   On command:
#
#   Description:
#------------------------------------------------------------------
    def On(self):
        self.debug_stream("In %s::On()"%(self.get_name()))
        # Add your own code here
        self.dp.DevSerWriteString('N\r') 

#------------------------------------------------------------------
#   ResetInterlocks command:
#
#   Description:
#------------------------------------------------------------------
    def ResetInterlocks(self):
        self.debug_stream("In %s::ResetInterlocks()"%(self.get_name()))
        #  Add your own code here
        self.dp.DevSerWriteString('RS\r')
        self.cleanAllImportantLogs()

#------------------------------------------------------------------
#   updateState command:
#
#   Description:
#------------------------------------------------------------------

    def UpdateStatus(self):
        self.debug_stream("In %s::UpdateStatus()"%(self.get_name()))
        if self.dp == None:
            self._linkSerial()
            self.warn_stream("Trying to relink with the serial port.")
            return

        #FIXME: this can be improved without use string conversion!
        try:
#            status = []
            self._errors = []
            stateCode64 = self.read_StateCode64()
            if stateCode64 == None:
                self.statusString = ''
            else:
                self.statusString = bin(stateCode64)[2:]#remove the '0b' header
                while len(self.statusString) < len(self.statusDict): self.statusString = '0'+self.statusString
            #start with the interpretation of the first bit of the state
            if self.statusString == '':
                self.cleanAllImportantLogs()
                self.changeState(PyTango.DevState.FAULT)
                self.addStatusMsg("Serial line is not responding",False)
                if self._haveSerialLineConn:
                    self.append2SerialLineTrace("Serial line is not responding")
                    self._haveSerialLineConn = False
            elif len(self.statusString) > 0 and self.statusString[0] == '0':
                #self._errors.append(self.statusDict[0][0]+' : '+self.statusDict[0][1][1]+'.')
                #adjusting state of the device is necessary
                if self.get_state() != PyTango.DevState.ON:
                    self.changeState(PyTango.DevState.ON)
                if not self._haveSerialLineConn:
                    self.append2SerialLineTrace("Serial line recovered")
                    self._haveSerialLineConn = True
            else:
                self._errors.append(self.statusDict[0][0]+' : '+self.statusDict[0][1][0]+'.')
                if self.get_state() != PyTango.DevState.OFF:
                    self.changeState(PyTango.DevState.OFF)
                if not self._haveSerialLineConn:
                    self.append2SerialLineTrace("Serial line recovered")
                    self._haveSerialLineConn = True
            #when first element processed, proceed with the next ones
            for i in range(1, len(self.statusString)):
                if i in [0, 3, 8, 13, 15, 23]:
                    continue#avoid this positions, are unused
                if i in [1,2,5,6]:
                    continue#avoid this positions, they are not errors
                if self.statusString[i]=='1':
                    self._errors.append(self.statusDict[i][0]+' : '+self.statusDict[i][1][0]+'.')
            #recollect the messaging
            if len(self._errors) > 0:
                self.push_change_event('Errors',self._errors)

            mode = self.isRemoteMode()
            if not mode == self._remoteMode:
                self._remoteMode = mode
                self.push_change_event('RemoteMode',self._remoteMode)
        except:
            self.error_stream('UpdateStatus() method failed!: %s' % (traceback.format_exc()))

#------------------------------------------------------------------
#   ReadAD command:
#
#   Description:
#------------------------------------------------------------------
    def ReadAD(self,argin):
        self.debug_stream("In %s::ReadAD(%d)"%(self.get_name(),int(argin)))
        self.dp.DevSerWriteString('AD %d\r'%int(argin))
        self.waiting=threading.Event()
        self.waiting.wait(0.1) 
        res = self.dp.DevSerReadRaw().strip('\n\r')
        if not self._IsAnswerError(res):
            return res
        else:
            return "None"

###
# auxiliar methods
    def isRemoteMode(self):
        self.dp.DevSerWriteString('CMD\r')
        self.waiting=threading.Event()
        self.waiting.wait(0.1) 
        res = self.dp.DevSerReadRaw().strip('\n\r')
        if not self._IsAnswerError(res):
            if res=='REM ': return True
            else: return False
        else: return None

    def _IsAnswerError(self,answer):
        if answer == '':
            self.warn_stream('Void answer from the Power Converter')
            return True
        if answer.startswith('?/x07'):
            error = res.split(' ',1)[1]
            self.error_stream('Power Converter say: %s'%(error))
            return True
        return False

    def changeState(self,newstate):
        self.debug_stream("In %s::changeState(%s)"%(self.get_name(),str(newstate)))
        self.set_state(newstate)
        self.addStatusMsg()
        self.push_change_event('State',newstate)
        #When state changes, clean the previous non important logs.
        self.addStatusMsg("") 
    def fireEventsList(self,eventsAttrList):
        timestamp = time.time()
        for attrEvent in eventsAttrList:
            try:
                self.debug_stream("In %s::fireEventsList() attribute: %s"%(self.get_name(),attrEvent[0]))
                if len(attrEvent) == 3:#specifies quality
                    self.push_change_event(attrEvent[0],attrEvent[1],timestamp,attrEvent[2])
                else:
                    self.push_change_event(attrEvent[0],attrEvent[1],timestamp,PyTango.AttrQuality.ATTR_VALID)
            except Exception,e:
                self.error_stream("In %s::fireEventsList() Exception with attribute %s"%(self.get_name(),attrEvent[0]))
                print e
    #@todo: clean the important logs when they loose importance.
    def cleanAllImportantLogs(self):
        self.debug_stream("In %s::cleanAllImportantLogs()"%self.get_name())
        self._important_logs = []
        self.addStatusMsg("")
    def addStatusMsg(self,current=None,important = False):
        try:
            self.info_stream("In %s::addStatusMsg()"%self.get_name())
            msg = "The device is in %s state.\n"%(self.get_state())
            for ilog in self._important_logs:
                msg = "%s%s\n"%(msg,ilog)
            if not current == None and not current == "":
                status = "%s%s\n"%(msg,current)
            else: status = "%s"%(msg)
            self.set_status(status)
            self.push_change_event('Status',status)
            if important and not current in self._important_logs:
                self._important_logs.append(current)
        except Exception,e:
            self.error_stream("Exception adding status message: %s"%(e))
            traceback.format_exc()
    def append2SerialLineTrace(self,msg):
        self._serialLineTrace.append("%s: %s"%(time.ctime(),msg))
        while len(self._serialLineTrace) >= 32:
            self._serialLineTrace.pop()
        self.push_change_event('SerialLineTrace',self._serialLineTrace)

# end auxiliar methods
###

#==================================================================
#
#   DanfysikClass class definition
#
#==================================================================
class DanfysikClass(PyTango.DeviceClass):

    # Class Properties
    class_property_list = {
        }

    #  Device Properties
    device_property_list = {
        'SerialLine': [PyTango.DevString,'The serial line to be used.',
                       #['alba02:10000/lab/ct/dctmoxa07-04']],#No alba's lab default value
                       ['']],
        }

    #  Command definitions
    cmd_list = {
        'Off':
            [[PyTango.DevVoid, ""],
            [PyTango.DevVoid, ""]],
        'On':
            [[PyTango.DevVoid, ""],
            [PyTango.DevVoid, ""]],
        'ResetInterlocks':
            [[PyTango.DevVoid, ""],
            [PyTango.DevVoid, ""]],
        'UpdateStatus':
            [[PyTango.DevVoid, ""],
            [PyTango.DevVoid, ""]],
        'ReadAD':
            [[PyTango.DevShort,""],
            [PyTango.DevString,""]],
        }

    #  Attribute definitions
    attr_list = {
        'Current':
            [[PyTango.DevDouble,
            PyTango.SCALAR,
            PyTango.READ],
            {
                'unit':'A',
            } ],
        'CurrentSetpoint':
            [[PyTango.DevDouble,
            PyTango.SCALAR,
            PyTango.READ_WRITE],
            {
                'unit':'A',
            } ],
        'Voltage':
            [[PyTango.DevDouble,
            PyTango.SCALAR,
            PyTango.READ],
            {
                'unit':'V',
            } ],
        'CurrentSlewrate':
            [[PyTango.DevDouble,
            PyTango.SCALAR,
            PyTango.READ_WRITE],
            {
                'unit':'A/s',
            } ],
        'AmbientTemperature':
            [[PyTango.DevDouble,
            PyTango.SCALAR,
            PyTango.READ],
            {
                'unit':'Celsius',
            } ],
        'StateCode64':
            [[PyTango.DevLong64,
            PyTango.SCALAR,
            PyTango.READ]],
        'RemoteMode':
            [[PyTango.DevBoolean,
            PyTango.SCALAR,
            PyTango.READ_WRITE]],
        'Errors':
            [[PyTango.DevString,
            PyTango.SPECTRUM,
            PyTango.READ,32],
             ],
        'SerialLineTrace':
            [[PyTango.DevString,
            PyTango.SPECTRUM,
            PyTango.READ,32],
            {
             'description':'Collect traces about the serial line cuts and recovers'
            }
             ]
        }

#------------------------------------------------------------------
#   DanfysikClass Constructor
#------------------------------------------------------------------
    def __init__(self, name):
        PyTango.DeviceClass.__init__(self, name)
        self.set_type(name);
        print "In DanfysikClass constructor"

#==================================================================
#
#   Danfysik class main method
#
#==================================================================
if __name__ == '__main__':
    try:
        py = PyTango.Util(sys.argv)
        py.add_TgClass(DanfysikClass,Danfysik,'Danfysik')

        U = PyTango.Util.instance()
        U.server_init()
        U.server_run()

    except PyTango.DevFailed,e:
        print '-------> Received a DevFailed exception:',e
    except Exception,e:
        print '-------> An unforeseen exception occured....',e
